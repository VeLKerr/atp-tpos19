#! /usr/bin/env bash

apt update && apt upgrade -y
apt install git mc vim htop python3-dev python3-pip salt-minion salt-syndic -y
