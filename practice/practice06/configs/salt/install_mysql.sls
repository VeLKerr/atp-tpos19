mysql_install_and_run:
  pkg.installed:
    - name: mysql-server
  service.running:
    - name: mysql
    - enable: True
