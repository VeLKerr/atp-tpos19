#! /usr/bin/env bash

curl -sSL https://get.docker.com/ | sudo sh
for us in `ls /home/`; do sudo usermod -aG docker ${us}; done
apt-add-repository ppa:ansible/ansible -y
apt update && apt upgrade -y
apt install ansible git mc vim htop python3-dev python3-pip -y
