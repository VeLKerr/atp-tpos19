## Локальный запуск

Запуск: 
```
vagrant plugin install vagrant-hostmanager
vagrant plugin install hostmanager
vagrant up
```. 
* velkerr-vm1 - "мастер", на нём установлен ansible & docker.

Доступ на машинки: ssh velkerr-vm1, ssh velkerr-vm2, velkerr-vm3. 
* Пользователь vagrant
* Пароль: vagrant

Проверка, что хосты пингуются из мастера: `ansible -m ping all -i /etc/ansible/hosts --ask-pass`.

Если не работает и валится по key-fingerprint, нужно из vm1 сделать заход по ssh на остальные машинки:
```
ssh velkerr-vm2
ssh velkerr-vm3
```

Если не хочется каждый раз вводить пароль, делаем следующее:

```bash
for num in `seq 2 3`; do ssh-copy-id vagrant@velkerr-vm${num}; done
```

Если пишет "no identifiers found", на vm1 нужно сгенерировать ключ: `ssh-keygen -q`.

Команда спросит пароль 2 раза, после чего между машинками можно будет ходить без пароля и ansible вызывать без `--ask-pass`.

## Удалённый запуск

Если локально не поднимается, идём на сервер.

`ssh tpo2020XX@193.200.211.74 -p 1171`,
* значения XX смотрим в этой [таблице](https://docs.google.com/spreadsheets/d/16wD4h4iNqBubPlW36F_3CmyE59zDbymc1LcK-gTwH4Y/edit#gid=0).
* пароль: vGWitTrkavPCU2juesBl

#### Проблема с sudo

Vagrant должен внести имена вновь созданных машинок в /etc/hosts, но сделать это без sudo не получится. Делаем следующее:

1. Коментим строку `vm3.vm.provision :shell, :inline => $hosts_script` для каждой машинки в vagrantfile
2. Изменяем IP у своих виртуалок (чтоб не совпадали с IP ваших коллег)
2. Перезапускаем создание машин
3. Дальше работаем не с именами VM, а с IP (их смотрим в Vagrantfile).


## Настройка Apache

```
- hosts: all
  tasks:
    - name: Install apache
      apt: pkg=apache2 state=latest update_cache=true
```

Если не работает, учим польователя становиться рутом:
```
- hosts: all
  become: yes
  become_user: root
  tasks:
    - name: Install apache
      apt: pkg=apache2 state=latest update_cache=true
```

Этот конфиг пишем в файл с расшиением *.yml.

Далее вызываем: `ansible-playbook -i /etc/ansible/hosts file.yml`

### Копируем файлы с пом. Ansible

Создаем такой файл **my-app.conf** на vm1:

```
  <VirtualHost *:80>
    DocumentRoot /var/www/my-app

    Options -Indexes

    ErrorLog /var/log/apache2/error.log
    TransferLog /var/log/apache2/access.log
  </VirtualHost>
```

Обновляем playbook.

```
- hosts: all
  become: yes
  become_user: root
  tasks:
    - name: Install apache
      apt: pkg=apache2 state=latest update_cache=true

    - name: Push future default virtual host configuration
      copy: src=my-app.conf dest=/etc/apache2/sites-available/ mode=0640

    - name: Activates our virtualhost
      command: a2ensite my-app

    - name: Check that our config is valid
      command: apache2ctl configtest

    - name: Deactivates the default virtualhost
      command: a2dissite default

    - name: Deactivates the default ssl virtualhost
      command: a2dissite default-ssl
      notify:
        - restart apache

  handlers:
    - name: restart apache
      service: name=apache2 state=restarted
```

Если не работает, идем на машинки и смотрим, какой файл лежит в пути `/etc/apache2/sites-available/`.

В `command: a2dissite default` меняем default на название файла (без conf). 

### Удалённая правка конфигов

Изменим порт, на котором будет работать наш "сайт". Для этого добавим ещё 2 task в ansible.

```
- name: apache2 listen on port 8081
      lineinfile: dest=/etc/apache2/ports.conf regexp="^Listen 80" line="Listen 8081" state=present
      notify:
        - restart apache2

- name: apache2 virtualhost on port 8081
      lineinfile: dest=/etc/apache2/sites-available/my-app.conf regexp="^<VirtualHost \*:80>" line="<VirtualHost *:8081>" state=present
      notify:
        - restart apache2
```

### Nginx
```
https://github.com/geerlingguy/ansible-role-nginx.git
```

Клонируем в roles/nginx

https://rtfm.co.ua/ansible-primer-ustanovki-nginx/
