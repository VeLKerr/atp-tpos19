#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from ConfigParser import ConfigParser
from setuptools import setup

ROOT_PACKAGE_NAME = 'hjudge'
CONF_FILE_NAME = 'global.conf'


def get_list_values(config, param_name):
    return config.get('GLOBAL', param_name).split(',')


def read_description():
    with open('README.md') as f:
        return f.read()


def update_packnames(packages):
    real_packages = [ROOT_PACKAGE_NAME]
    real_packages.extend([ROOT_PACKAGE_NAME + '.' + package for package in packages])
    return real_packages


config = ConfigParser()
config.readfp(open(os.path.join(os.getcwd(), CONF_FILE_NAME), 'r'))

setup(
    name=ROOT_PACKAGE_NAME,
    version=config.get('GLOBAL', 'version'),
    author=get_list_values(config, 'authors'),
    author_email=get_list_values(config, 'emails'),
    packages=update_packnames(['tests', 'inner_tests', 'system_tests', 'resource_tests', 'helpers']),
    package_data={'': ['*.dict', '*.list', 'hjudge.conf.default', '*.yml']},
    include_package_data=True,
    install_requires=['pydoop==1.2', 'python-dateutil (>= 2.6)', 'yarn_api_client==0.2.3',
                      'ipython', 'nbconvert', 'pyyaml (>= 3.12)'],
    description='Computer-aided system for applications in Hadoop ecosystem',
    long_description=read_description(),
    license='GNU LGPL',
    keywords=['BigData', 'QA'],
    zip_safe=False,
    # url='http://dublin.vdi.mipt.ru:9999/VeLKerr/hjudge.git',  # only from MIPT local network!
    download_url='http://dublin.vdi.mipt.ru:9999/VeLKerr/hjudge.git/tarball/master',
    entry_points={'console_scripts': ['hjudge = hjudge.main:main']},
    platforms=["Linux"],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Topic :: BigData :: MapReduce',
        'Topic :: BigData :: QA',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7'
    ]
)

